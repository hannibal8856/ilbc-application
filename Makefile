
TARGET:=voip

SRCS:=voip.c
CODEC_DIR:=iLBC

CFLAGS:= -Wall -pthread

all: $(CODEC_DIR) $(SRCS)
	make -C $(CODEC_DIR)
	$(CC) $(CFLAGS) $(SRCS) -o $(TARGET)

clean:
	rm -rf $(TARGET) *.o
	make -C $(CODEC_DIR) $@

.PHONY: all clean
